const a = {
    "took" : 39,
    "timed_out" : false,
    "_shards" : {
      "total" : 1,
      "successful" : 1,
      "skipped" : 0,
      "failed" : 0
    },
    "hits" : {
      "total" : {
        "value" : 699,
        "relation" : "eq"
      },
      "max_score" : 1.0,
      "hits" : [
        {
          "_index" : "jobs",
          "_type" : "article",
          "_id" : "xnC_pnEBpZfAQ849OBd8",
          "_score" : 1.0,
          "_source" : {
            "title" : "Senior IT Infrastructure Analyst / IT Infrastructure Analyst (Ref. No.: L20/04-0061)",
            "company_name" : "Link Asset Management Limited",
            "location" : "Kowloon Bay",
            "job_info" : "Degree holderMinimum 5 years of working experiences in IT fieldSolid experience in large-scale of IT project",
            "url" : "<a href='https://hk.jobsdb.com/hk/en/job/senior-it-infrastructure-analyst-it-infrastructure-analyst-100003007752081?searchRequestToken=ccf2f050-e6f5-40a0-80fc-05d0b1e5f7c1&sectionRank=25&jobId=100003007752081'></a>"
          }
        },
        {
          "_index" : "jobs",
          "_type" : "article",
          "_id" : "x3C_pnEBpZfAQ849OBd8",
          "_score" : 1.0,
          "_source" : {
            "title" : "Planning Engineer (Project based contract) (Ref: 20/001)",
            "company_name" : "Kum Shing Group",
            "location" : "Hong Kong SAR",
            "job_info" : "Minimum 5 years working experience on scheduling and planning, experience in Process Plant Erection is highly preferable; Experience in ...",
            "url" : "https://hk.linkedin.com/jobs/view/planning-engineer-project-based-contract-ref-20-001-at-kum-shing-group-1829964971?refId=dade9447-0b82-4868-9ca3-e97df0c0953f&position=2&pageNum=5&trk=public_jobs_job-result-card_result-card_full-click"
          }
        },
        {
          "_index" : "jobs",
          "_type" : "article",
          "_id" : "yHC_pnEBpZfAQ849OBd8",
          "_score" : 1.0,
          "_source" : {
            "title" : "Assistant Relationship Manager - Large Corporates",
            "company_name" : "E.SUN Commercial Bank, Ltd.",
            "location" : "Tsim Sha Tsui",
            "job_info" : "Five-day work weekDouble payPerformance bonus",
            "url" : "<a href='https://hk.jobsdb.com/hk/en/job/assistant-relationship-manager-large-corporates-100003007751830?searchRequestToken=b5e1f97b-f508-424b-9269-1e2cf50a3eb1&sectionRank=212&jobId=100003007751830'></a>"
          }
        }
      ]
    }
  }

  console.log(a.hits.hits.map(h => h._source));