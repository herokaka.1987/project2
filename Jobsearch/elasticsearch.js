var Knex = require('knex');
// const Alldatabase = require ('./webService/serviceWeb')

(function () {
    'use strict';
    

    const fs = require('fs');
    const elasticsearch = require('elasticsearch');
    const esClient = new elasticsearch.Client({
        host: '127.0.0.1:9200',
        log: 'error'
    });

    const bulkIndex = function bulkIndex(index, type, data) {
        let bulkBody = [];

        data.forEach(item => {
            bulkBody.push({
                index: {
                    _index: index,
                    _type: type,
                    _id: item.id
                }
            });

            bulkBody.push(item);
        });

        esClient.bulk({ body: bulkBody })
            .then(response => {
                let errorCount = 0;
                response.items.forEach(item => {
                    if (item.index && item.index.error) {
                        console.log(++errorCount, item.index.error);
                    }
                });
                console.log(`Successfully indexed ${data.length - errorCount} out of ${data.length} items`);
            })
            .catch(console.err);
    };

    // only for testing purposes
    // all calls should be initiated through the module
    const test = function test() {
        const knexConfig = require("./knexfile");
        const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
        const articlesRaw = (knex.raw(/*sql*/`SELECT title, company_name, location, job_info, url FROM jobsdb UNION SELECT title, company_name, location, job_info, url FROM indeed UNION SELECT title, company_name, location, job_info, url FROM linkedin;`)).rows;
        const articles = JSON.parse(result);
        console.log(`${articles.length} items parsed from data file`);
        bulkIndex('jobs', 'article', articles);
    };

    test();

    module.exports = {
        bulkIndex
    };
}());