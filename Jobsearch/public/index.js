async function main() {
	const x = document.getElementById("search");
	const p = x.value;
	
	const res = await fetch(encodeURI(`/jobjobsearch/search?q=${p}`));

	const data = await res.json();
	

	const dataset = data.hits.hits.map(h => h._source)
	document.querySelector('.row').innerHTML = '';
	for (let i = 0; i < dataset.length; i++) {
		const datas = dataset[i]
		
		let memoHTML = '<div class="col-md-2">';
		

		if (datas.website == "linkedin") {
			memoHTML += `<div class="card body"><img src="https://i.ibb.co/b65fRTh/linkedin.png" alt="linkedin" border="0">`
		} else if (datas.website == "indeed") {
		memoHTML += `<div class="card body"><img src="https://i.ibb.co/t36JKgh/image.jpg" alt="image" border="0">`
		} else {
			memoHTML += `<div class="card body"><img src="https://i.ibb.co/Zf45YPs/image.png" alt="image" border="0">`
		}

		memoHTML += `<a href= ${datas.url}class="card-title" target="_blank">${datas.title}</a>`;
		memoHTML += '<h6 class="card-company">' + datas.company_name + '</h6> ';
		memoHTML += '<p class="card-location">' + datas.location + '</p> ';
		memoHTML += '<p class="card-info">' + datas.job_info + '</p> ';

		memoHTML += '</div>'

		document.querySelector('.row').innerHTML += memoHTML;

	}
}


async function Hidelogout() {
    await fetch('/login/google').then(function(response) {
      if(response.ok) {
        document.getElementById("signout").style.display = "block";
      }
    }
  )}
  Hidelogout();