import Knex from "knex";


async function main() {
    const knexConfig = require("./knexfile");
    const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

    try {
        const test = await knex.raw(/*SQL*/ `SELECT job_info FROM indeed`);
        console.log(test);
    } catch (err) {
        console.log(err.message);
    } finally {
        await knex.destroy();
      }    
}

main();