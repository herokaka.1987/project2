import express from 'express'
// import bodyParser from 'body-parser'
import path from 'path'
import { serviceWeb } from './webService/serviceWeb'
import { indeedRouter } from './webRouter/indeed_router'
import { jobsdbRouter } from './webRouter/jobsdb_router'

//<-------google------>>//
import { isLoggedInHtml } from './guards';
import grant from 'grant-express';
import dotenv from 'dotenv';
dotenv.config();
import { userService } from "./userService/UserService"
import fetch from 'node-fetch';

//<-------google------>>//
import expressSession from "express-session";

//<-------elasticsearch------>>//
// import { search } from './search_all'

const app = express()

// app.use(bodyParser.urlencoded({ extended: false }))

const ServiceWeb = new serviceWeb;
const indeedWeb = new indeedRouter(ServiceWeb);
const jobsdbWeb = new jobsdbRouter(ServiceWeb);

const API_VERSION = "/jobjobsearch"
app.use(`${API_VERSION}/indeed`, indeedWeb.router());
app.use(`${API_VERSION}/jobsdb`, jobsdbWeb.router());

app.get(`${API_VERSION}/search`, async (req, res) => {

  const myRes = await fetch(encodeURI(`http://jobjobsearch.cyou:9200/jobs/_search?q=${req.query.q}&size=30&from=1`));
  const data = await myRes.json();
  res.json(data);
})

app.get(`${API_VERSION}/membersearch`, async (req, res) => {

  const myRes = await fetch(encodeURI(`http://jobjobsearch.cyou:9200/jobs/_search?q=${req.query.q}&size=50&from=${req.query.from}`));
  const data = await myRes.json();
  res.json(data);
})


app.use(
  expressSession({
    secret: "project2~job",
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false }
  })
);

app.use(grant({
  "defaults": {
    "protocol": "https",
    "host": "jobjobsearch.cyou",
    "transport": "session",
    "state": true,
  },
  "google": {
    "key": process.env.GOOGLE_CLIENT_ID || "",
    "secret": process.env.GOOGLE_CLIENT_SECRET || "",
    "scope": ["profile", "email"],
    "callback": "/users/login/google"
  },
}));

import { userRouter } from "./userRouter/UserRouter"
// import { stringify } from 'querystring'
const UserService = new userService();
const UserRouter = new userRouter(UserService)
app.use("/users", UserRouter.router());
app.use(express.static(path.join(__dirname, './public')))
app.use(isLoggedInHtml, express.static(path.join(__dirname, './prviate')));



const PORT = 8080;
app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
});
