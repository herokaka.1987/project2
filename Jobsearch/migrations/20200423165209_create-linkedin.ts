import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable("linkedin",table=>{
        table.increments("linkedin_id");
        table.string("title").notNullable();
        table.string("company_name");
        table.string("location");
        table.text("job_info").notNullable();
        table.text("url").notNullable();
        table.string("website").notNullable();
    })
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable("linkedin");
}

