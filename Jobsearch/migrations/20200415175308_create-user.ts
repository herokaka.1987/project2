import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable("google_login", table => {
        table.increments("user_id");
        table.string("username").notNullable();
        table.string("password").notNullable();
        table.timestamps(false, true);
    });

}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable("google_login");
}

