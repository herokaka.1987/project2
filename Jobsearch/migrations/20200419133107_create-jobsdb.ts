import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable("jobsdb",table=>{
        table.increments("jobsdb_id");
        table.string("title").notNullable();
        table.string("company_name").notNullable();
        table.string("location");
        table.string("job_info");
        table.text("url").notNullable();
        table.string("website").notNullable();
    
    })
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable("jobsdb");
}

