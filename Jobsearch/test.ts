import Knex from 'knex';
import puppeteer from 'puppeteer';
import cheerio from 'cheerio';
// import jsonfile from 'jsonfile';
async function test2() {
    await (async () => {
        const browser = await puppeteer.launch({
            // headless: false,
            args: [`–no - sandbox`, `–disable - setuid - sandbox`],
            ignoreDefaultArgs: [`–disable - extensions`]
        })

        const knexConfig = require("./knexfile");
        const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
        await knex.raw(/* sql */`DELETE FROM jobsdb`);
        for (let p = 1; p < 200; p++) {


            const page = await browser.newPage();
            await page.setDefaultNavigationTimeout(0);
            await page.goto('https://hk.jobsdb.com/hk/en/Search/FindJobs?JSRV=1&page=' + p);

            console.log(p)

            // await page.waitForSelector('#intercom-container')
            await page.waitFor(2 * 1000);

            let body = await page.content()

            let $ = await cheerio.load(body)

            const memos = (await knex.raw(/* sql */`SELECT * FROM jobsdb`)).rows;
            await $('article').each((i, el) => {
                // console.log(el)

                const $el = $(el);
                memos.push
                    (
                        {
                            title: $el.find('h1>a').text().trim(),
                            company_name: $el.find('a._58veS_1my').eq(0).text().trim(),
                            location: $el.find('a._58veS_1my').eq(1).text().trim(),
                            job_info: $el.find('._3RqUb_1my').eq(0).text().trim() + $el.find('._3RqUb_1my').eq(1).text().trim() + $el.find('._3RqUb_1my').eq(2).text().trim(),
                            url: $el.find('h1>a').attr('href'),
                            website: "jobsdb",

                        });




            })
            console.log(memos)
            // console.log("The file was saved!");

            // await jsonfile.writeFile('./jobs.json', memos);
            // const knexConfig = require("./knexfile");
            // const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
            await knex.raw(/* sql */`DELETE FROM jobsdb`);

            for (let i = 0; i < memos.length; i++) {
                await knex.raw(/*sql*/`INSERT INTO jobsdb 
                (title, company_name, location, job_info, url, website) VALUES (?,?,?,?,?,?)`
                    , [memos[i].title, memos[i].company_name, memos[i].location, memos[i].job_info, memos[i].url, memos[i].website]);
            }


        }
        await knex.destroy()
        await browser.close()


    })();

    await (async () => {
        const browser = await puppeteer.launch({
            // headless: false,
            args: [`–no - sandbox`, `–disable - setuid - sandbox`],
            ignoreDefaultArgs: [`–disable - extensions`]
        })

        const knexConfig = require("./knexfile");
        const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
        await knex.raw(/* sql */`DELETE FROM linkedin`);

        for (let p = 0; p < 2000; p += 25) {


            const page = await browser.newPage();
            await page.setDefaultNavigationTimeout(0);
            await page.goto('https://www.linkedin.com/jobs-guest/jobs/api/seeMoreJobPostings/search?geoId=103291313&location=%E9%A6%99%E6%B8%AF%E7%89%B9%E5%88%A5%E8%A1%8C%E6%94%BF%E5%8D%80&start=' + p);

            console.log(p)
            await page.waitFor(2 * 1000);
            // await page.waitForSelector('.job-result-card__right-corner', {visible: true })
            //     await page.waitForSelector('.job-result-card__right-corner' , {
            //         timeout: 1000
            //   })
            // await page.waitForSelector(selector,{timeout:1000})

            let body = await page.content()

            let $ = await cheerio.load(body)
            // console.log(body)

            const memos = (await knex.raw(/* sql */`SELECT * FROM linkedin`)).rows;

            await $('li').each((i, el) => {
                // console.log(el)

                const $el = $(el);
                memos.push
                    (
                        {
                            title: $el.find('div>h3').text().trim(),
                            company_name: $el.find('div>h4').text().trim(),
                            location: $el.find('.job-result-card__location').text().trim(),
                            job_info: $el.find('.job-result-card__snippet').text().trim(),
                            url: $el.find('.result-card__full-card-link').attr('href'),
                            website: "linkedin",

                        });




            })
            console.log(memos)
            await knex.raw(/* sql */`DELETE FROM linkedin`);
            for (let i = 0; i < memos.length; i++) {
                await knex.raw(/*sql*/`INSERT INTO linkedin 
                (title, company_name, location, job_info, url, website) VALUES (?,?,?,?,?,?)`
                    , [memos[i].title, memos[i].company_name, memos[i].location, memos[i].job_info, memos[i].url, memos[i].website]);
            }


        }
       await knex.destroy()
        await browser.close()


    })();

    await (async () => {
        const browser = await puppeteer.launch({
            // headless: false,
            args: [`–no - sandbox`, `–disable - setuid - sandbox`],
            ignoreDefaultArgs: [`–disable - extensions`]
        })

        const knexConfig = require("./knexfile");
        const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
        await knex.raw(/* sql */`DELETE FROM indeed`);

        for (let p = 0; p < 2000; p += 10) {


            const page = await browser.newPage();
            await page.setDefaultNavigationTimeout(0);
            await page.goto(`https://hk.indeed.com/jobs?l=hong+kong&sort=date&start=${p}`);

            console.log(p)

            await page.waitForSelector('.mosaic-zone')

            let body = await page.content()

            let $ = await cheerio.load(body)

            const memos = (await knex.raw(/* sql */`SELECT * FROM indeed`)).rows;

            await $('#resultsCol .jobsearch-SerpJobCard').each((i, el) => {
                // console.log(el)

                const $el = $(el);
                memos.push
                    (
                        {
                            title: $el.find('h2>a').eq(0).text().trim(),
                            company_name: $el.find('div>span').eq(0).text().trim(),
                            location: $el.find('.location').eq(0).text().trim(),
                            job_info: $el.find('.summary').text().trim(),
                            url: `https://hk.indeed.com${$el.find('h2>a').attr('href')}`,
                            website: "indeed",

                        });




            })
            // console.log(p)
            // console.log("The file was saved!");

            // await jsonfile.writeFile('./jobs.json', memos);
            // const knexConfig = require("../knexfile");
            // const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
            await knex.raw(/* sql */`DELETE FROM indeed`);
            console.log(memos);

            for (let i = 0; i < memos.length; i++) {
                await knex.raw(/*sql*/`INSERT INTO indeed 
                    (title, company_name, location, job_info, url, website) VALUES (?,?,?,?,?,?)`
                    , [memos[i].title, memos[i].company_name, memos[i].location, memos[i].job_info, memos[i].url, memos[i].website]);
            }

        }
        await knex.destroy()
        await browser.close()

    })();

    await (function () {
        'use strict';


        // const fs = require('fs');
        const elasticsearch = require('elasticsearch');
        const esClient = new elasticsearch.Client({
            host: 'http://jobjobsearch.cyou:9200',
            log: 'error'
        });


        const bulkIndex = function bulkIndex(index: string, nGram: string, data: any[]) {
            let bulkBody: { index: { _index: string; _type: string; _id: string; }; }[] = [];

            data.forEach(item => {
                bulkBody.push({
                    index: {
                        _index: index,
                        _type: nGram,
                        _id: item.id
                    }
                });

                bulkBody.push(item);
            });

            esClient.bulk({ body: bulkBody })
                .then((response: { items: any[]; }) => {
                    let errorCount = 0;
                    response.items.forEach((item: { index: { error: any; }; }) => {
                        if (item.index && item.index.error) {
                            console.log(++errorCount, item.index.error);
                        }
                    });
                    console.log(`Successfully indexed ${data.length - errorCount} out of ${data.length} items`);
                })
                .catch(console.error);

        };


        // only for testing purposes
        // all calls should be initiated through the module
        const test = async function test() {
            console.log('Indexes have been deleted!')
            await esClient.indices.delete({
                index: 'jobs'
            })

            const knexConfig = require("./knexfile");
            const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
            const result = (await knex.raw(/*sql*/`SELECT title, company_name, location, job_info, url, website FROM jobsdb UNION SELECT title, company_name, location, job_info, url , website FROM indeed UNION SELECT title, company_name, location, job_info, url , website FROM linkedin;`)).rows;
            const json = JSON.stringify(result)
            const articles = JSON.parse(json);
            console.log(`${articles.length} items parsed from data file`);
            bulkIndex('jobs', 'article', articles);
            await knex.destroy();
            
        };

        test();


        

        module.exports = {
            bulkIndex
        };
    }());
}

test2();