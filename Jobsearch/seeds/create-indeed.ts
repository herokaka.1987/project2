// import * as Knex from "knex";

// export async function seed(knex: Knex): Promise<any> {
//     // Deletes ALL existing entries
//     return knex("table_name").del()
//         .then(() => {
//             // Inserts seed entries
//             return knex("table_name").insert([
//                 { id: 1, colName: "rowValue1" },
//                 { id: 2, colName: "rowValue2" },
//                 { id: 3, colName: "rowValue3" }
//             ]);
//         });
// };

import * as Knex from "knex";
import jsonfile from 'jsonfile';

exports.seed = async function (knex: Knex) {
    await knex.raw(/* sql */`DELETE FROM indeed`);

    const indeed_jobs = await jsonfile.readFile('./indeed.json');
    // const questionMark = "(?,?,?,?)";
    for(let i = 0; i < indeed_jobs.length; i++){
        await knex.raw(/*sql*/`INSERT INTO indeed 
        (title, company_name, location, job_info) VALUES (?,?,?,?)`
        , [indeed_jobs[i].title, indeed_jobs[i].company_name, indeed_jobs[i].location, indeed_jobs[i].job_info]);
    }
    
};
