import cheerio from 'cheerio';
import Knex from 'knex';
import jsonfile from 'jsonfile';

import puppeteer from 'puppeteer';
// import fs from 'fs';



(async () => {
    const browser = await puppeteer.launch({
        headless: true
    })
    let num = 0;
    for (let p = 0; p < 1500; p+=25 ){


        const page = await browser.newPage();
        await page.goto('https://www.linkedin.com/jobs-guest/jobs/api/seeMoreJobPostings/search?keywords=&location=Hong%2BKong%2C%2BHong%2BKong%2BSAR&trk=homepage-jobseeker_jobs-search-bar_search-submit&start='+p);

        // console.log(p)
        await page.waitFor(2*1000);
        // await page.waitForSelector('.job-result-card__right-corner', {visible: true })
    //     await page.waitForSelector('.job-result-card__right-corner' , {
    //         timeout: 1000
    //   })
        // await page.waitForSelector(selector,{timeout:1000})

        let body = await page.content()

        let $ = await cheerio.load(body)
        // console.log(body)
        const knexConfig = require("../knexfile");
        const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
        const memos = (await knex.raw(/* sql */`SELECT * FROM linkedin`)).rows;
        await $('li').each((i, el) => {
            // console.log(el)
            num++;
            const $el = $(el);
            memos.push
                (
                    {
                        title: $el.find('div>h3').text().trim(),
                        company_name: $el.find('div>h4').text().trim(),
                        location: $el.find('.job-result-card__location').text().trim(),
                        job_info: $el.find('.job-result-card__snippet').text().trim(),
                        url: $el.find('.result-card__full-card-link').attr('href'),
                        linkedin_num: num,

                    });
            console.log(num);



        })
        // console.log(p)
        // console.log("The file was saved!");

        await jsonfile.writeFile('./jobs.json', memos);
        // const knexConfig = require("../knexfile");
        // const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
        await knex.raw(/* sql */`DELETE FROM linkedin`);

        for (let i = 0; i < memos.length; i++) {
            await knex.raw(/*sql*/`INSERT INTO linkedin 
            (title, company_name, location, job_info, url, linkedin_num) VALUES (?,?,?,?,?,?)`
                , [memos[i].title, memos[i].company_name, memos[i].location, memos[i].job_info, memos[i].url, memos[i].linkedin_num]);
        }
        await knex.destroy();

    }
    await browser.close()


})();


    // await browser.close()


    // async function retrieve() {
    //     const knexConfig = require("./knexfile");
    //     const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
    //     await knex.raw(/* sql */`DELETE FROM jobsdb`);

    //     // const indeed_jobs = await jsonfile.readFile('./jobs.json');
    //     for (let i = 0; i < indeed_jobs.length; i++) {
    //         await knex.raw(/*sql*/`INSERT INTO indeed 
    //     (title, company_name, location, job_info, url, indeed_num) VALUES (?,?,?,?,?,?)`
    //             , [indeed_jobs[i].title, indeed_jobs[i].company_name, indeed_jobs[i].location, indeed_jobs[i].job_info, indeed_jobs[i].url, indeed_jobs[i].indeed_num]);
    //     }
    //     await knex.destroy();
    // };

    // retrieve();

// })();