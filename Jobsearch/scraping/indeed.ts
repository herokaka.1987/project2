import puppeteer from 'puppeteer';
import cheerio from 'cheerio';
import Knex from 'knex';
import jsonfile from 'jsonfile';


// import fs from 'fs';



(async () => {
    const browser = await puppeteer.launch({
        headless: true
    })
    let num = 0;
    for (let p = 0; p < 1000; p += 10) {


        const page = await browser.newPage();
        await page.goto(`https://hk.indeed.com/jobs?l=hong+kong&sort=date&start=0${p}`);

        // console.log(p)

        await page.waitForSelector('.mosaic-zone')

        let body = await page.content()

        let $ = await cheerio.load(body)
        const knexConfig = require("../knexfile");
        const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
        const memos = (await knex.raw(/* sql */`SELECT * FROM indeed`)).rows;
        await $('#resultsCol .jobsearch-SerpJobCard').each((i, el) => {
            // console.log(el)
            num++;
            const $el = $(el);
            memos.push
                (
                    {
                        title: $el.find('h2>a').eq(0).text().trim(),
                        company_name: $el.find('div>span').eq(0).text().trim(),
                        location: $el.find('.location').eq(0).text().trim(),
                        job_info: $el.find('.summary').text().trim(),
                        url: `<a href='https://hk.indeed.com${$el.find('h2>a').attr('href')}'></a>`,
                        indeed_num: num,

                    });
            console.log(num);



        })
        // console.log(p)
        // console.log("The file was saved!");

        await jsonfile.writeFile('./jobs.json', memos);
        // const knexConfig = require("../knexfile");
        // const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
        await knex.raw(/* sql */`DELETE FROM indeed`);
        // console.log(memos);
        
        for (let i = 0; i < memos.length; i++) {
            await knex.raw(/*sql*/`INSERT INTO indeed 
                (title, company_name, location, job_info, url, indeed_num) VALUES (?,?,?,?,?,?)`
                , [memos[i].title, memos[i].company_name, memos[i].location, memos[i].job_info, memos[i].url, memos[i].indeed_num]);
        }
        await knex.destroy();
    }
    await browser.close()

})();

