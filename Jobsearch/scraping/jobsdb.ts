import puppeteer from 'puppeteer';
import cheerio from 'cheerio';
import Knex from 'knex';
import jsonfile from 'jsonfile';


// import fs from 'fs';



(async () => {
    const browser = await puppeteer.launch({
        headless: true
    })
    let num = 0;
    for (let p = 1; p < 50; p++) {


        const page = await browser.newPage();
        await page.goto('https://hk.jobsdb.com/hk/en/Search/FindJobs?JSRV=1&page=' + p);

        // console.log(p)

        await page.waitForSelector('#intercom-container')

        let body = await page.content()

        let $ = await cheerio.load(body)
        const knexConfig = require("../knexfile");
        const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
        const memos = (await knex.raw(/* sql */`SELECT * FROM jobsdb`)).rows;
        await $('article').each((i, el) => {
            // console.log(el)
            num++;
            const $el = $(el);
            memos.push
                (
                    {
                        title: $el.find('h1>a').text().trim(),
                        company_name: $el.find('span._3CTQy').eq(0).text().trim(),
                        location: $el.find('span._3CTQy').eq(1).text().trim(),
                        job_info: $el.find('._3RqUb_1my').eq(0).text().trim() + $el.find('._3RqUb_1my').eq(1).text().trim() + $el.find('._3RqUb_1my').eq(2).text().trim(),
                        // highlight2: $el.find('._3RqUb_1my').eq(1).text().trim(),
                        // highlight: $el.find('._3RqUb_1my').eq(2).text().trim(),
                        url: `<a href='${$el.find('h1>a').attr('href')}'></a>`,
                        jobsdb_num: num,

                    });
            console.log(num);



        })
        // console.log(p)
        // console.log("The file was saved!");

        await jsonfile.writeFile('./jobs.json', memos);
        // const knexConfig = require("../knexfile");
        // const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
        await knex.raw(/* sql */`DELETE FROM jobsdb`);

        for (let i = 0; i < memos.length; i++) {
            await knex.raw(/*sql*/`INSERT INTO jobsdb 
                (title, company_name, location, job_info, url, jobsdb_num) VALUES (?,?,?,?,?,?)`
                , [memos[i].title, memos[i].company_name, memos[i].location, memos[i].job_info, memos[i].url, memos[i].jobsdb_num]);
        }
        await knex.destroy();

    }
    await browser.close()


})();


    // await browser.close()


    // async function retrieve() {
    //     const knexConfig = require("./knexfile");
    //     const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
    //     await knex.raw(/* sql */`DELETE FROM jobsdb`);

    //     // const indeed_jobs = await jsonfile.readFile('./jobs.json');
    //     for (let i = 0; i < indeed_jobs.length; i++) {
    //         await knex.raw(/*sql*/`INSERT INTO indeed 
    //     (title, company_name, location, job_info, url, indeed_num) VALUES (?,?,?,?,?,?)`
    //             , [indeed_jobs[i].title, indeed_jobs[i].company_name, indeed_jobs[i].location, indeed_jobs[i].job_info, indeed_jobs[i].url, indeed_jobs[i].indeed_num]);
    //     }
    //     await knex.destroy();
    // };

    // retrieve();

// })();

