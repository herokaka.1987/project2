import puppeteer from 'puppeteer';
import cheerio from 'cheerio';
// import Knex from 'knex';
import jsonfile from 'jsonfile';


// import fs from 'fs';



(async () => {
    const browser = await puppeteer.launch({
        headless: true
    })
    let num = 0;
    for (let p = 1; p < 6; p++) {


        const page = await browser.newPage();
        await page.goto('https://www.ctgoodjobs.hk/ctjob/listing/joblist.asp?page=' + p);

        // console.log(p)

        await page.waitForSelector('.no-print')

        let body = await page.content()

        let $ = await cheerio.load(body)
        const memos = await jsonfile.readFile('./jobs.json')
        await $('.row jl-row').each((i, el) => {
            // console.log(el)
            num++;
            const $el = $(el);
            memos.push
                (
                    {
                        title: $el.find('h2>a').text().trim(),
                        company_name: $el.find('h3>a').eq(0).text().trim(),
                        location: $el.find('loc col-xs-6').eq(1).text().trim(),
                        job_info: $el.find('.summary').text().trim(),
                        url: $el.find('h1>a').attr('href'),
                        jobsdb_num: `jobsdb_${num}`,

                    });
            console.log(num);



        })
        // console.log(p)
        // console.log("The file was saved!");

        await jsonfile.writeFile('./jobs.json', memos);
        // const knexConfig = require("../knexfile");
        // const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
        // await knex.raw(/* sql */`DELETE FROM jobsdb`);

        // for (let i = 0; i < memos.length; i++) {
        //     await knex.raw(/*sql*/`INSERT INTO jobsdb 
        //         (title, company_name, location, highlight3, highlight2, highlight, url, jobsdb_num) VALUES (?,?,?,?,?,?,?,?)`
        //         , [memos[i].title, memos[i].company_name, memos[i].location, memos[i].highlight3, memos[i].highlight2, memos[i].highlight, memos[i].url, memos[i].jobsdb_num]);
        // }
        // await knex.destroy();

    }
    await browser.close()


})();


    // await browser.close()


    // async function retrieve() {
    //     const knexConfig = require("./knexfile");
    //     const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
    //     await knex.raw(/* sql */`DELETE FROM jobsdb`);

    //     // const indeed_jobs = await jsonfile.readFile('./jobs.json');
    //     for (let i = 0; i < indeed_jobs.length; i++) {
    //         await knex.raw(/*sql*/`INSERT INTO indeed 
    //     (title, company_name, location, job_info, url, indeed_num) VALUES (?,?,?,?,?,?)`
    //             , [indeed_jobs[i].title, indeed_jobs[i].company_name, indeed_jobs[i].location, indeed_jobs[i].job_info, indeed_jobs[i].url, indeed_jobs[i].indeed_num]);
    //     }
    //     await knex.destroy();
    // };

    // retrieve();

// })();

