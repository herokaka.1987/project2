import {Request,Response,NextFunction} from  'express'

export function isLoggedInHtml (req:Request,res:Response,next :NextFunction){

    if(req.session?.user){
        // console.log("step isLoggedIn TRUE");
    // console.log(req.session?.user)

        next();
    }else{
        res.redirect("/index.html")
    }
}


export function isLoggedInApi(req:Request,res:Response,next :NextFunction){
    if(req.session?.user){
        // console.log("step isLoggedIn TRUE");
    // console.log(req.session?.user)

        next();
    }else{
        res.status(401).json({message:"你冇登入喎"})
}
}
    

