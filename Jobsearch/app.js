var request = require('request');
let cheerio = require('cheerio');
let fs = require('fs');
let jsonfile = require('jsonfile');



// function getAllPage() {

// }

for (let i = 0; i < 100; i += 10) {
    function getAllCard() {
        return new Promise((resolve, reject) => {
            var options = {
                'method': 'GET',
                'url': `https://hk.indeed.com/jobs?l=hong%20kong&sort=date&start=${i}`,
            };
            console.log(options);
            request(options, function (error, response, body) {
                if (error) reject();
                var $ = cheerio.load(body)
                let card = $('#resultsCol .jobsearch-SerpJobCard').map((index, obj) => {
                    return {
                        title: $(obj).find('h2>a').eq(0).text().trim(),
                        company_name: $(obj).find('div>span').eq(0).text().trim(),
                        location: $(obj).find('.location').eq(0).text().trim(),
                        job_info: $(obj).find('.summary').text().trim(),

                    }
                }).get()
                resolve(card);
            });
        })
    } getAllCard().then((card) => {
        // console.log(card);
        // console.log(card.length);
        const content = JSON.stringify(card); //轉換成json格式
        // const memos = jsonfile.readFile('./indeed.json')
        // memos.push({ content });
        // console.log(content);
        // memos.push(card);

        // jsonfile.writeFile('./indeed.json');
        
        fs.writeFile("indeed.json", content, 'utf8', function (err) {
            if (err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        });
    })
}


