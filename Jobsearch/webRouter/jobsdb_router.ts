import express from 'express';
import { Request, Response } from 'express';
import { serviceWeb } from '../webService/serviceWeb';
import Knex from 'knex';
import puppeteer from 'puppeteer';
import cheerio from 'cheerio';
// import bulkIndex from '../elasticsearch';


export class jobsdbRouter {
    constructor(private SerivceWeb: serviceWeb) { }

    router() {
        const router = express.Router();
        router.get("/jobsdb", this.getjobsdbJobs)
        // router.post("/job", this.getWebPage)
        router.get("/job", this.getWebPage)
        router.get("/jobsdb_db", this.getjobsdbDatabase)
        return router;

    }


    getjobsdbJobs = async (req: Request, res: Response) => {
        try {

            const memos = await this.SerivceWeb.getjobsdbJobs()


            res.json(memos)


        } catch (e) {
            console.error(e);
            res.status(500).json({ result: false });
        }
    }

    getWebPage = async (req: Request, res: Response) => {
        // const memos = await this.SerivceWeb.getWebPage()
        try {

            (async () => {
                const browser = await puppeteer.launch({
                    headless: true
                })
                let num = 0;
                for (let p = 1; p < 6; p++) {


                    const page = await browser.newPage();
                    await page.goto('https://hk.jobsdb.com/hk/en/Search/FindJobs?JSRV=1&page=' + p);

                    console.log(p)

                    await page.waitForSelector('#intercom-container')

                    let body = await page.content()

                    let $ = await cheerio.load(body)
                    const memos = await this.SerivceWeb.getjobsdbDatabase();
                    await $('article').each((i, el) => {
                        // console.log(el)
                        
                        num++;
                        const $el = $(el);
                        memos.push
                            (
                                {
                                    title: $el.find('h1>a').text().trim(),
                                    company_name: $el.find('div.mCDH2_1my').eq(0).text().trim(),
                                    location: $el.find('div.mCDH2_1my').eq(1).text().trim(),
                                    highlight3: $el.find('._3RqUb_1my').eq(0).text().trim(),
                                    highlight2: $el.find('._3RqUb_1my').eq(1).text().trim(),
                                    highlight: $el.find('._3RqUb_1my').eq(2).text().trim(),
                                    url: $el.find('h1>a').attr('href'),
                                    jobsdb_num: `jobsdb_${num}`,

                                });
                    
                    console.log(memos)
                    // console.log("The file was saved!");

                

                })
                
                

                
                    const knexConfig = require("../knexfile");
                    const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
                    await knex.raw(/* sql */`DELETE FROM jobsdb`);
                    
                    for (let i = 0; i < memos.length; i++) {
                        await knex.raw(/*sql*/`INSERT INTO jobsdb 
                    (title, company_name, location, highlight3, highlight2, highlight, url, jobsdb_num) VALUES (?,?,?,?,?,?,?,?)`
                            , [memos[i].title, memos[i].company_name, memos[i].location, memos[i].highlight3, memos[i].highlight2, memos[i].highlight, memos[i].url, memos[i].jobsdb_num]);
                    }
                    await knex.destroy();
                
            };
            await browser.close()
                
            })()
        } catch (e) {
            console.error(e);
            res.status(500).json({ result: false });
        }
    }

    getjobsdbDatabase = async (req: Request, res: Response) => {
        try {

            const db_data = await this.SerivceWeb.getjobsdbDatabase()

            res.json(db_data)


        } catch (e) {
            console.error(e);
            res.status(500).json({ result: false });
        }
    }
}
