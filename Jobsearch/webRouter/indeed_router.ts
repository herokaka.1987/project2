import express from 'express';
import { Request, Response } from 'express';
import { serviceWeb } from '../webService/serviceWeb';
import Knex from 'knex';
import puppeteer from 'puppeteer';
import cheerio from 'cheerio';
import { Client, ApiResponse, RequestParams } from '@elastic/elasticsearch'
const client = new Client({ node: 'http://localhost:9200' })
// import { bulkIndex } from '../elasticsearch'


export class indeedRouter {
    constructor(private SerivceWeb: serviceWeb) { }

    router() {
        const router = express.Router();
        router.get("/indeed", this.getindeedJobs)
        // router.post("/job", this.getWebPage)
        router.get("/job", this.getWebPage)
        router.get("/search", this.elasticsearch)
        router.get("/indeed_db", this.getindeedDatabase)
        return router;

    }


    getindeedJobs = async (req: Request, res: Response) => {
        try {

            const memos = await this.SerivceWeb.getindeedJobs()


            res.json(memos)


        } catch (e) {
            console.error(e);
            res.status(500).json({ result: false });
        }
    }

    getWebPage = async (req: Request, res: Response) => {
        // const memos = await this.SerivceWeb.getWebPage()
        try {

            (async () => {
                const browser = await puppeteer.launch({
                    headless: true
                })
                let num = 0;
                const memos = await this.SerivceWeb.getindeedDatabase();
                for (let p = 0; p < 100; p += 10) {


                    const page = await browser.newPage();
                    await page.goto(`https://hk.indeed.com/jobs?l=hong+kong&sort=date&start=${p}`);

                    // console.log(p)

                    await page.waitForSelector('.mosaic-zone')

                    let body = await page.content()

                    let $ = await cheerio.load(body)
                    // const memos = await this.SerivceWeb.getindeedDatabase();
                    await $('#resultsCol .jobsearch-SerpJobCard').each((i, el) => {
                        // console.log(el)

                        num++;
                        const $el = $(el);
                        memos.push
                            (
                                {
                                    title: $el.find('h2>a').eq(0).text().trim(),
                                    company_name: $el.find('div>span').eq(0).text().trim(),
                                    location: $el.find('.location').eq(0).text().trim(),
                                    job_info: $el.find('.summary').text().trim(),
                                    url: $el.find('h2>a').attr('href'),
                                    indeed_num: `indeed_${num}`,

                                });

                                console.log(num);

                    })
                    
                    
                    
                    // console.log("The file was saved!");

                    const knexConfig = require("../knexfile");
                    const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
                    await knex.raw(/* sql */`DELETE FROM indeed`);

                    for (let i = 0; i < memos.length; i++) {
                        await knex.raw(/*sql*/`INSERT INTO indeed 
                    (title, company_name, location, job_info, url, indeed_num) VALUES (?,?,?,?,?,?)`
                            , [memos[i].title, memos[i].company_name, memos[i].location, memos[i].job_info, memos[i].url, memos[i].indeed_num]);
                    }
                    await knex.destroy();
                };
                await browser.close()

                
            })()
        } catch (e) {
            console.error(e);
            res.status(500).json({ result: false });
        }
    }

    getindeedDatabase = async (req: Request, res: Response) => {
        try {

            const db_data = await this.SerivceWeb.getindeedDatabase()

            res.json(db_data)


        } catch (e) {
            console.error(e);
            res.status(500).json({ result: false });
        }
    }

    elasticsearch = async (req: Request, res: Response) => {
        try {
            
                // Let's start by indexing some data
                const doc1: RequestParams.Index = {
                  index: 'library',
                  body: {
                    character: 'indeed_job',
                    quote: 'project2'
                  }
                }
                await client.index(doc1)
              
                // Let's search!
                const params: RequestParams.Search = {
                  index: 'library',
                  body: {
                    query: {
                      match: {
                        quote: 'jobjobsearch'
                      }
                    }
                  }
                }
                client
                  .search(params)
                  .then((result: ApiResponse) => {
                    console.log(result.body.hits.hits)
                    res.json(result.body.hits.hits);
                  })
                  .catch((err: Error) => {
                    console.log(err)
                  })
                  
              
              
              
        } catch (e) {
            console.error(e);
            res.status(500).json({ result: false });
        }
    }
}
