import path from 'path'
import jsonfile from 'jsonfile'
// import pg from 'pg'
import Knex from 'knex'
import puppeteer from 'puppeteer';

export class serviceWeb {

    protected serviceDatabase: string
    private indeedJsonPath: string
    private jobsdbJsonPath: string

    constructor() {
        this.serviceDatabase = require("../knexfile");
        this.indeedJsonPath = path.join(__dirname, "../indeed.json")
        this.jobsdbJsonPath = path.join(__dirname, "../jobsdb.json")
    }

    async getindeedJobs() {
        const work = await jsonfile.readFile(this.indeedJsonPath)
        // console.log(work)
        return work
    }

    async getindeedDatabase() {
        // const results = [];
        
        const knexConfig = this.serviceDatabase
        const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
        const result = (await knex.raw(/*sql*/`SELECT * FROM indeed;`)).rows;
        // results.push(result);
        // await knex.destroy();
        // console.log(results);
        return result;
    }

    async getWebPage() {
        const memos = await puppeteer.launch
        return memos
    }

    async getjobsdbJobs() {
        const work = await jsonfile.readFile(this.jobsdbJsonPath)
        // console.log(work)
        return work
    }

    async getjobsdbDatabase() {
        // const results = [];
        
        const knexConfig = this.serviceDatabase
        const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
        const result = (await knex.raw(/*sql*/`SELECT * FROM jobsdb;`)).rows;
        // results.push(result);
        // await knex.destroy();
        // console.log(results);
        return result;
    }

    async getAllDatabase() {
        // const results = [];
        
        const knexConfig = this.serviceDatabase
        const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
        const result = (await knex.raw(/*sql*/`SELECT title, company_name, location, job_info, url FROM jobsdb UNION SELECT title, company_name, location, job_info, url FROM indeed UNION SELECT title, company_name, location, job_info, url FROM linkedin;`)).rows;
        // results.push(result);
        // await knex.destroy();
        // console.log(results);
        return result;
    }

}